const test = document.getElementById('test');
const result = document.getElementById('result');

let startTime, endTime;

test.addEventListener('click', function () {
  test.style.background = '#444';
  test.style.pointerEvents = 'none';
  startTime = new Date();

  fetch('https://speed.hetzner.de/100MB.bin')
    .then(response => response.blob())
    .then(blob => {
      endTime = new Date();
      const timeTaken = (endTime - startTime) / 1000;
      const speedMbps = ((blob.size / timeTaken) * 8) / 1000000;
      result.innerText = `Your internet speed is ${speedMbps.toFixed(2)} Mbps`;
      test.style.background = '#ccc';
      test.style.pointerEvents = 'auto';
    })
    .catch(error => {
      console.log(error);
      test.style.background = '#ccc';
      test.style.pointerEvents = 'auto';
    });
});
